Source: ros-ros-control
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Thomas Moulard <thomas.moulard@gmail.com>,
           Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends: debhelper-compat (= 12), catkin, ros-message-generation, libstd-msgs-dev, python3-all (>= 2.6.6-3), python3-setuptools (>= 0.6b3), dh-python, libcontrol-toolbox-dev, libresource-retriever-dev, liburdfdom-dev, pluginlib-dev, librostest-dev, ros-std-msgs, liburdf-dev
Standards-Version: 4.5.0
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/ros_control
Vcs-Browser: https://salsa.debian.org/science-team/ros-ros-control
Vcs-Git: https://salsa.debian.org/science-team/ros-ros-control.git

Package: libcontroller-interface-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libhardware-interface-dev, pluginlib-dev
Description: Development files for controller_interface (Robot OS)
 This package contains development files for Interface base class for controllers.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libcontroller-manager-dev
Section: libdevel
Architecture: any
Depends: libcontroller-manager0d (= ${binary:Version}), ${misc:Depends}, libcontroller-interface-dev, libcontroller-manager-msgs-dev, librealtime-tools-dev
Description: Development files for controller_manager (Robot OS)
 This package contains development files for the controller manager.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libcontroller-manager0d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Runtime files for controller_manager (Robot OS)
 The controller_manager provides a hard-realtime-compatible loop to control a
 robot mechanism, as well as infrastructure to load, unload, start and stop
 controllers. 
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.


Package: python3-controller-manager
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python files for controller_manager (Robot OS)
 The controller_manager provides a hard-realtime-compatible loop to control
 a robot mechanism, as well as infrastructure to load, unload, start and stop  
 controllers. This package contains the Python binding.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libcontroller-manager-tests-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libcontroller-manager-dev, libcontroller-interface-dev, libcontrol-toolbox-dev
Description: Development files for controller_manager_tests (Robot OS)
 This package contains development files for controller manager_tests.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libcontroller-manager-tests
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Plugin files for controller_manager_tests (Robot OS)
 This package contains the controller manager_tests library.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: python3-controller-manager-tests
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python files for controller_manager_tests (Robot OS)
 This package contains the Python binding for controller manager_tests library.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libcontroller-manager-msgs-dev
Section: libdevel
Architecture: all
Depends: ${misc:Depends}, ros-message-runtime, libstd-msgs-dev
Description: C/C++ headers for controller_manager Robot OS Messages
 This package is part of Robot OS (ROS). Messages and services for the
 controller manager.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: python3-controller-manager-msgs
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-std-msgs
Description: Python code for controller_manager Robot OS Messages
 This package is part of Robot OS (ROS). Messages and services for the
 controller manager. This package contains the generated Python package.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: cl-controller-manager-msgs
Section: lisp
Architecture: all
Depends: ${misc:Depends}, cl-std-msgs
Description: LISP code for controller_manager Robot OS Messages
 This package is part of Robot OS (ROS). Messages and services for the
 controller manager. This package contains the generated LISP library.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libhardware-interface-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libroscpp-dev
Description: Development files for hardware_interface (Robot OS)
 This package is contains development fils for hardware Interface base class.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libjoint-limits-interface-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libhardware-interface-dev
Description: Development files for joint_limits_interface (Robot OS)
 This package constains the interface for enforcing joint limits.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: python3-rqt-controller-manager
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Python code for joint_limits_interface (Robot OS)
 This package contains the generated Python package with the interface for 
 enforcing joint limits.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The   
 packages are generic controllers to all robots. They takes as input the joint 
 state data from your robot's actuator's encoders and an input set point. It
 uses a generic control loop feedback mechanism.

Package: libtransmission-interface-dev
Section: libdevel
Architecture: any
Depends: libtransmission-interface0d (= ${binary:Version}), ${misc:Depends}, libhardware-interface-dev, ros-cmake-modules, pluginlib-dev, libresource-retriever-dev, libroscpp-dev
Description: Development files for transmission_interface (Robot OS)
 Development files for transmission Interface library. The library
 contains data structures for representing mechanical transmissions, and 
 methods for propagating position, velocity and effort variables between 
 actuator and joint spaces.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The
 packages are generic controllers to all robots. They takes as input the joint
 state data from your robot's actuator's encoders and an input set point. It  
 uses a generic control loop feedback mechanism

Package: libtransmission-interface0d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Runtime files for transmission_interface (Robot OS)
 The transmission Interface library. The library contains data structures 
 for representing mechanical transmissions, and methods for propagating 
 position, velocity and effort variables between actuator and joint spaces.
 .
 This package is part of Robot OS (ROS) and the project ros_control. The
 packages are generic controllers to all robots. They takes as input the joint
 state data from your robot's actuator's encoders and an input set point. It  
 uses a generic control loop feedback mechanism

